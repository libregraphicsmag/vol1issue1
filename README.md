# First encounters, taking flight — November 2010 Issue 1.1, Libre Graphics magazine

## Index

* **If, at first...**, ginger coon — *Editor's letter*
* *Production colophon*, Manufactura Independente
* **Make Art 2010** — *Notebook*
* *New releases*
* **Freed fonts, strong web**, Dave Crossland — *Column*
* **This is the first day of my life**, Eric Schrijver — *Column*
* **F/LOSS in the classroom**, Ludivine Loiseau — *Dispatch*
* **The unicorn tutorial**, ginger coons — *First time*
* **Pierre Marchand talks FontMatrix, responsiveness and user engagement**, Dave Crossland interviews Pierre Marchand — *Interview*
* **Camouflage and mimicry illuminated drawing**, Laura C. Hewitt — *Showcase*
* Pete Meadows — *Showcase*
* John LeMasney — *Showcase*
* **Applying F/LOSS as a final user and not dying in the attempt**, Lila Pagola — *Feature*
* **Visual literacy: knowing through images**, Eric Schrijver — *Visual Feature*
* **Interview with Ben Laenen of DejaVu**, ginger coons interviews Ben Laenen — *Feature*
* *Resource list*
* *Glossary*

## Colophon

First encounters, taking flight — November 2010  
Issue 1.1, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416
