This is the layout document for Libre Graphics Magazine, vol 1.1.

We are using Scribus NG 1.3.8.
Please use this very same version of Scribus to open/edit the document.
You can get it at http://www.scribus.net/node/224

We are using PropCourier Sans (from the 'typeface' folder);
Linux Libertine and Linux Biolinum (available at http://www.linuxlibertine.org/index.php?id=91&L=1).
Please install these typefaces before opening the layout document.

Finally, we're using 2 files for the layout; after outputting in PDF, use the pdftk tool to merge them together:
pdftk *.pdf cat output lgmag1.pdf

